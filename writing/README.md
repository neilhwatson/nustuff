Simplicity - lose the extra words. Bracket parts that might not be needed and read around them. Does it still make sense?

First person - use I, even if the goal is to no be I, start with I then remove it later.

Audience - write for yourself whenever possible. Say what you want to say. Even in technical prose, where the goal is to inform the audience, the audience can be a less informed you.

Aloud - hear the rhythm of the sentence and how words sound together.

Unity - Pronound, Tense, Mood

Subject - What point do I want to make? It doesn't have to be the final point. I just have to state my point well. Leave the reader with just one thought.
